<?php
namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Birthday extends DB
{
    public $id;
    public $name;
    public $birthdate;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "This is Birthday class!";
    }

    public function setData ($postVariabledata=NULL){
        if (array_key_exists("id",$postVariabledata)){
            $this->id    = $postVariabledata ['id'];
        }
        if (array_key_exists("name",$postVariabledata)){
            $this->name    = $postVariabledata ['name'];
        }
        if (array_key_exists("birthdate",$postVariabledata)){
            $this->birthdate    = $postVariabledata ['birthdate'];
        }

    }
    public function store(){
        $arrData = array($this->name,$this->birthdate);
        $sql ="insert into  birthday(name, birthdate) VALUES (?,?)";
        $STH =$this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if ($result)
            Message::message("success!data inserted success :) ");
        else{
            Message::message("Failed!data has not inserted success :( ");
            }
        Utility::redirect('create.php');
    }//end of store()


}//end of Book_Title calss
