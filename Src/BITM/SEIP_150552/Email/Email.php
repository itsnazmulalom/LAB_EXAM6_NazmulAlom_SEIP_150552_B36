<?php
namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Email extends DB
{
    public $id;
    public $name;
    public $emailaddress;
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "This is Email class!";
    }

    public function setData ($postVariabledata=NULL){
        if (array_key_exists("id",$postVariabledata)){
            $this->id    = $postVariabledata ['id'];
        }
        if (array_key_exists("name",$postVariabledata)){
            $this->name   = $postVariabledata ['name'];
        }
        if (array_key_exists("emailaddress",$postVariabledata)){
            $this->emailaddress   = $postVariabledata ['emailaddress'];
        }

    }
    public function store(){
        $arrData = array($this->name,$this->emailaddress);
        $sql ="insert into  email(name, emailaddress) VALUES (?,?)";
        $STH =$this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if ($result)
            Message::message("success!data inserted success :) ");
        else{
            Message::message("Failed!data has not inserted success :( ");
        }
        Utility::redirect('create.php');
    }//end of store()


}//end of Book_Title calss
