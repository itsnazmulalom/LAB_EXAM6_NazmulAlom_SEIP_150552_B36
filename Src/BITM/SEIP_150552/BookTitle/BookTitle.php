<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;

  public function __construct()
  {
      parent::__construct();
  }




    public function setData ($postVariabledata=NULL){
        if (array_key_exists("id",$postVariabledata)){
            $this->id    = $postVariabledata ['id'];
        }
        if (array_key_exists("book_title",$postVariabledata)){
            $this->book_title    = $postVariabledata ['book_title'];
        }
        if (array_key_exists("author_name",$postVariabledata)){
            $this->author_name    = $postVariabledata ['author_name'];
        }

    }
    public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql ="insert into  book_title(book_title, author_name) VALUES (?,?)";
        $STH =$this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if ($result)
            Message::message("success!data inserted success :) ");
        else{
            Message::message("Failed!data has not inserted success :( ");
        }
        Utility::redirect('create.php');
    }//end of store()

    //this is for view data
    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(\PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(\PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(\PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(\PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();




}//end of Book_Title calss





