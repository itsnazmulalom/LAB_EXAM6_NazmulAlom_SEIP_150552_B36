<?php
namespace App\profilepic;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class profilepic extends DB
{
    public $id;
    public $name;
    public $profilepic;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "This is profilepic class!";
    }

    public function setData ($postVariabledata=NULL){
        if (array_key_exists("id",$postVariabledata)){
            $this->id    = $postVariabledata ['id'];
        }
        if (array_key_exists("name",$postVariabledata)){
            $this->name   = $postVariabledata ['name'];
        }
        if (array_key_exists("profilepic",$postVariabledata)){
            $this->profilepic   = $postVariabledata ['profilepic'];
        }

    }
    public function store(){
        $arrData = array($this->name,$this->profilepic);
        $sql ="insert into  gender(name,profilepic) VALUES (?,?)";
        $STH =$this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if ($result)
            Message::message("success!data inserted success :) ");
        else{
            Message::message("Failed!data has not inserted success :( ");
        }
        Utility::redirect('create.php');
    }//end of store()


}//end of Book_Title calss
