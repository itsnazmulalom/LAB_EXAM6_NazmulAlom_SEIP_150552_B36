<?php
namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Gender extends DB
{
    public $id;
    public $name;
    public $gender;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo "This is Email class!";
    }

    public function setData ($postVariabledata=NULL){
        if (array_key_exists("id",$postVariabledata)){
            $this->id    = $postVariabledata ['id'];
        }
        if (array_key_exists("name",$postVariabledata)){
            $this->name   = $postVariabledata ['name'];
        }
        if (array_key_exists("gender",$postVariabledata)){
            $this->gender   = $postVariabledata ['gender'];
        }

    }
    public function store(){
        $arrData = array($this->name,$this->gender);
        $sql ="insert into  gender(name, gender) VALUES (?,?)";
        $STH =$this->DBH->prepare($sql);
        $result =$STH->execute($arrData);

        if ($result)
            Message::message("success!data inserted success :) ");
        else{
            Message::message("Failed!data has not inserted success :( ");
        }
        Utility::redirect('create.php');
    }//end of store()


}//end of Book_Title calss
